#!/bin/bash
echo "installing OAI eNB..."
cd ~
git clone https://gitlab.eurecom.fr/oai/openairinterface5g
cd ~/openairinterface5g
source oaienv
cd ~/openairinterface5g/cmake_targets
sudo cp /local/repository/etc/rrc_eNB.c ~/openairinterface5g/openair2/RRC/LTE/rrc_eNB.c
sudo ./build_oai -I -c -C --eNB -w USRP